<?php

use Illuminate\Database\Seeder;
Use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        User::create([
            'name' => 'konaesan',
            'email' => 'eliseothniel@gmail.com',
            'password' => bcrypt('admin'),
        ]);
    }
}
