<?php

return [
    'nom' => 'Last name',
    'prenom' => 'First name',
    'titremodification' => 'Modify',
    'titreaffichage' => 'View',
    'msgmiseajourok' => 'Update successful',
    'msgenregistrementok' => 'Add successful',
    'msgsupprimerok' => 'Delete successful',
];