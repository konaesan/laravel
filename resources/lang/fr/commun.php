<?php

return [
    'details' => 'Voir',
    'modifier' => 'Editer',
    'enregistrer' => 'Enregistrer',
    'voir' => 'Afficher',
    'supprimer' => 'Supprimer',
    'acceuil' => 'Acceuil',
    'langue' => 'Langue',
];