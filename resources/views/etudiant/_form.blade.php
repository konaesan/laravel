@if (isset($etudiant))
    {!! Form::model($etudiant, ['route' => ['updateEtudiant', $etudiant->id], 'method' => 'put']) !!}
@else
    {!! Form::open(['route' => 'addEtudiant']) !!}
@endif


{!! Form::label('nom', trans('etudiant.nom')) !!}

{!! Form::text('nom') !!}

{!! Form::label('prenom', trans('etudiant.prenom')) !!}

{!! Form::text('prenom') !!}

{!! Form::submit(trans('commun.enregistrer'), ['class' => 'btn btn-sm btn-primary m-t-n-xs']) !!}

{!! Form::close() !!}
